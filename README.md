![Gopher](https://i.imgur.com/glk34vC.png)

# Status Check API

[![pipeline status](https://gitlab.com/skyvet/health-check-monitor/badges/master/pipeline.svg)](https://gitlab.com/skyvet/health-check-monitor/commits/master)

## What is this?

This program will constantly check for one or more http endpoints and notify you whenever something goes down and also when it goes up again!

It will additionally expose an api with the current statuses of every endpoint configured.

## Configuration

This health check needs a `config.json` file with this minimal structure

```json
{
  "urlMonitors": [
    {
      "name": "Google",
      "url": "https://www.google.com"
    }
  ]
}
```  

You can also specify the config file path with the `-config` flag. For example:

```bash
health-check-monitor -config my/custom/dir/my_config.json
```

Check more configuration options in the conf.json [distribution file](https://gitlab.com/skyvet/health-check-monitor/raw/master/config.json.dist)

## Run locally

```bash
go get gitlab.com/skyvet/health-check-monitor
cd $GOPATH/src/gitlab.com/skyvet/health-check-monitor
go install
curl https://gitlab.com/skyvet/health-check-monitor/raw/master/config.json.dist -o config.json
health-check-monitor
```

## Run with docker

```bash
curl https://gitlab.com/skyvet/health-check-monitor/raw/master/config.json.dist -o config.json
docker run -d \
-v "$(pwd)/config.json:/config.json" \
-p "8001:8001" \
registry.gitlab.com/skyvet/health-check-monitor:0.1.0 -config /config.json
```

## Ping it!

```bash
curl http://localhost:8001/status
```
